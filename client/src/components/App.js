import React, {Component} from 'react'
import {Provider} from "mobx-react";

import Table from "./Table";
import Form from "./Form";
import TableDataList from "../models/TableDataList";


class App extends Component {
    store = new TableDataList();

    render() {
        return (
            <Provider store={this.store}>
                <div className="container">
                    <Table/>
                    <Form/>
                </div>
            </Provider>
        )
    }
}

export default App;

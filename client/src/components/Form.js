import React, {Component} from 'react'
import {observable, action} from "mobx";
import {inject, observer} from "mobx-react";


@inject("store")
@observer
class Form extends Component {
    @observable newTableName = "";
    @observable newTableJob = "";

    render() {
        return (
            <form>
                <label>Name</label>
                <input
                    type="text"
                    name="name"
                    value={this.newTableName}
                    onChange={this.handleInputChange}/>
                <label>Job</label>
                <input
                    type="text"
                    name="job"
                    value={this.newTableJob}
                    onChange={this.handleInputChange}/>
                <input type="button" value="Submit" onClick={this.handleFormSubmit}/>
            </form>
        );
    }

    @action
    handleInputChange = (event) => {
        const {name, value} = event.target;

        if (name === 'name') {
            this.newTableName = value;
        } else if (name === 'job') {
            this.newTableJob = value;
        }
    };

    @action
    handleFormSubmit = (event) => {
        this.props.store.addElementTable(this.newTableName, this.newTableJob);
        this.newTableName = "";
        this.newTableJob = "";
        event.preventDefault();
    };
}

export default Form;

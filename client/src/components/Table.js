import React, {Component} from 'react'
import {inject, observer} from "mobx-react";
import {action} from "mobx";


const TableHeader = () => {
    return (
        <thead>
        <tr>
            <th>Name</th>
            <th>Job</th>
            <th>Action</th>
        </tr>
        </thead>
    )
};

@inject("store")
@observer
class TableBody extends Component {
    render() {
        const rows = this.props.store.tableDataList.map(row => {
            return (
                <tr key={row.id}>
                    <td>{row.name}</td>
                    <td>{row.job}</td>
                    <td>
                        <button onClick={() => this.handleRemoveData(row.id)}>Delete</button>
                    </td>
                </tr>
            )
        });

        return <tbody>{rows}</tbody>
    }

    @action
    handleRemoveData = (removeId) => {
        this.props.store.removeElementTable(removeId);
    };
}

class Table extends Component {
    render() {
        return (
            <table>
                <TableHeader/>
                <TableBody/>
            </table>
        )
    }
}

export default Table

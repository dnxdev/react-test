import {observable} from "mobx";

class TableData {
    id = Math.random();
    @observable name = '';
    @observable job = '';

    constructor(name, job) {
        this.name = name;
        this.job = job;
    }
}

export default TableData

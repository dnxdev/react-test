import {action, observable} from "mobx";

import TableData from "./TableData";

class TableDataList {
    @observable tableDataList = [];

    @action
    addElementTable(name, job) {
        this.tableDataList.push(new TableData(name, job));
    }

    @action
    removeElementTable(removeId) {
        this.tableDataList = this.tableDataList.filter(tableData => {
            return tableData.id !== removeId
        });
    }
}

export default TableDataList